package com.example.demopro

import android.app.AlarmManager
import android.app.PendingIntent
import android.app.Service
import android.content.Context
import android.content.Intent
import android.media.MediaPlayer
import android.os.IBinder
import android.widget.Toast


class MyService: Service() {

    lateinit var myPlayer: MediaPlayer

    override fun onBind(intent: Intent?): IBinder? {
        return null
    }

    override fun onCreate() {
        Toast.makeText(this, "Service Created", Toast.LENGTH_LONG).show()
//        myPlayer = MediaPlayer.create(this, R.raw.file_example)
//        myPlayer.isLooping = false // Set looping
        super.onCreate()
    }

    override fun onStart(intent: Intent?, startId: Int) {
        /*var intents = Intent(baseContext, MainActivity::class.java)
        intents.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
        startActivity(intents)*/


        Toast.makeText(this, "Service Started", Toast.LENGTH_LONG).show()
//        myPlayer.start()
    }

    override fun onDestroy() {
        Toast.makeText(this, "Service Stopped", Toast.LENGTH_LONG).show()
//        myPlayer.stop()
    }

}