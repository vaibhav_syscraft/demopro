package com.example.demopro

import android.app.AlarmManager
import android.app.NotificationManager
import android.app.PendingIntent
import android.content.Context
import android.content.Intent
import android.os.Build
import android.os.Bundle
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.core.app.NotificationCompat
import androidx.work.Constraints
import androidx.work.OneTimeWorkRequest
import androidx.work.WorkManager
import com.example.demopro.databinding.ActivityMainBinding


class MainActivity : AppCompatActivity() {

    lateinit var mBinding: ActivityMainBinding

    lateinit var alarmManager: AlarmManager
    lateinit var pendingIntent:PendingIntent

    var started:Boolean = false

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        mBinding = ActivityMainBinding.inflate(layoutInflater)

        val constraints = Constraints.Builder().setRequiresBatteryNotLow(true).build()

        val workRequest = OneTimeWorkRequest.Builder(MyWorker::class.java).setConstraints(
            constraints
        ).build()

        mBinding.tvHello.setOnClickListener {
            //notificationDemo()
            //Enqueuing the work request
            WorkManager.getInstance().enqueue(workRequest)
        }

        mBinding.tvService.setOnClickListener {
            if (!started){
                started = true
                startService(Intent(this, MyService::class.java))
            }else{
                started = false
                stopService(Intent(this, MyService::class.java))
            }
        }

        //Listening to the work status
        WorkManager.getInstance().getWorkInfoByIdLiveData(workRequest.id)
            .observe(this,
                { t -> mBinding.textViewStatus.append(t.state.name + "\n") })


        alarmManager = getSystemService(Context.ALARM_SERVICE) as AlarmManager
        var alarmIntent = Intent(this, MyBroadCastReceiver::class.java)
        pendingIntent = PendingIntent.getBroadcast(this, 0, alarmIntent, 0)

        mBinding.btnStartAlarm.setOnClickListener {
            //startAlarm()
            var intent = Intent()
            intent.action = "com.tutorialspoint.CUSTOM_INTENT"
            sendBroadcast(intent)
        }

        mBinding.btnCancelAlarm.setOnClickListener {
            //cancelAlarm()
            var intent = Intent()
            intent.action = "com.tutorialspoint.CUSTOM_INTENT"
            sendBroadcast(intent)
        }

        setContentView(mBinding.root)
    }

    fun notificationDemo(){
        var builder = NotificationCompat.Builder(this@MainActivity, "my_channel_id_01")
                .setSmallIcon(R.drawable.ic_launcher_background)
                .setContentTitle("Notifications Example")
                .setContentText("This is a notification message")
                .setAutoCancel(true)
                .setPriority(NotificationCompat.PRIORITY_DEFAULT)

        var notificationIntent = Intent()
        var pendingIntent = PendingIntent.getActivities(
            this@MainActivity, 0,
            arrayOf(notificationIntent), PendingIntent.FLAG_UPDATE_CURRENT
        )
        builder.setContentIntent(pendingIntent)

        var manager = getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager
        manager.notify(0, builder.build())
    }


    fun startAlarm(){
        when {
            Build.VERSION.SDK_INT >= Build.VERSION_CODES.M -> {
                alarmManager.setAndAllowWhileIdle(AlarmManager.RTC_WAKEUP, 0, pendingIntent);
            }
            Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT -> {
                alarmManager.setExact(AlarmManager.RTC_WAKEUP, 0, pendingIntent);
            }
            else -> {
                alarmManager.set(AlarmManager.RTC_WAKEUP, 0, pendingIntent);
            }
        }
    }

    fun cancelAlarm(){
        alarmManager.cancel(pendingIntent)
        Toast.makeText(applicationContext, "Alarm Cancelled", Toast.LENGTH_LONG).show()
    }
}

//For google map location auto update
//https://stackoverflow.com/questions/41500765/how-can-i-get-continuous-location-updates-in-android-like-in-google-maps

//https://www.journaldev.com/27681/android-alarmmanager-broadcast-receiver-and-service