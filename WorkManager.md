WorkManager
======

WorkManager is part of Android Jetpack. WorkManager helps us to execute our tasks immediately or an appropriate time.

Earlier we had AlarmManager, JobScheduler, FirebaseJobDispatcher for scheduling the background tasks. But the issues were

* JobScheduler – Available only for API >= 21
* FirebaseJobDispatcher – For backward compatibility

WorkManager Features
* It is fully backward compatible so in your code you do not need to write if-else for checking the android version.
* With WorkManager we can check the status of the work.
* Tasks can be chained as well, for example when one task is finished it can start another.
* And it provides guaranteed execution with the constraints, we have many constrained available that we will see ahead.

### Understanding WorkManager Classes
Before writing the actual codes first we should understand the WorkManager classes.

- Worker: The main class where we will put the work that needs to be done.
- WorkRequest: It defines an individual task, like it will define which worker class should execute the task.
- WorkManager: The class used to enqueue the work requests.
- WorkInfo: The class contains information about the works. For each WorkRequest we can get a LiveData using WorkManager. The LiveData holds the WorkInfo and by observing it we can determine the Work Informations.